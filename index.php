<?php

echo "<h3>Output Akhir</h3>";

require('Animal.php');
require('frog.php');
require('ape.php'); 

$sheep = new Animal("shaun");

echo "Name = " .$sheep->name; // "shaun"
echo "<br>";
echo "Legs = ". $sheep->legs; // 4
echo "<br>";
echo "Cold Blooded = ".$sheep->cold_blooded; // "no"
echo "<br><br>";


$manuk = new frog("buduk");

echo "Name = " .$manuk->name; 
echo "<br>";
echo "Legs = ". $manuk->legs;
echo "<br>";
echo "Cold Blooded = ".$manuk->cold_blooded;
echo "<br>";
echo $manuk -> jump("hop hop");
echo "<br>";

$keraSakti = new ape("kera sakti");

echo "Name = " .$keraSakti->name; 
echo "<br>";
echo "Legs = ". $keraSakti->legs;
echo "<br>";
echo "Cold Blooded = ".$keraSakti->cold_blooded;
echo "<br>";
echo $keraSakti -> yell("Auooo");
echo "<br><br>";




?>

